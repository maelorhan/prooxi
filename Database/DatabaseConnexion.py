# -*- coding: utf-8 -*-

__author__ = 'Alexandre Cloquet'

import MySQLdb
import MySQLConnexion.MySQLConnexion


class DBProoxi():
    connexion = None

    def getUrlFromCatgory(self, categorie):
        categorieTmp = MySQLdb.escape_string(categorie)
        request = """   SELECT px_boutique.url
                        FROM
                            px_categorie INNER JOIN
                            px_cat_2_boutique ON px_categorie.id = px_cat_2_boutique.id_categorie
                        INNER JOIN
                            px_boutique ON px_boutique.id = px_cat_2_boutique.id_boutique
                        WHERE
                            px_categorie.categorie = \"""" + categorieTmp + """\" AND px_boutique.url != ""
                            AND px_boutique.url != "http://" """
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getUrlFromCatgoryImmobilier(self):
        request = """   SELECT px_boutique.url
                        FROM
                            px_categorie INNER JOIN
                            px_cat_2_boutique ON px_categorie.id = px_cat_2_boutique.id_categorie
                        INNER JOIN
                            px_boutique ON px_boutique.id = px_cat_2_boutique.id_boutique
                        WHERE
                            px_cat_2_boutique.id_categorie = 8 AND px_boutique.url != ""
                            AND px_boutique.url != "http://" """
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getUrlFromCatgoryVoyage(self):
        request = """   SELECT px_boutique.url
                        FROM
                            px_categorie INNER JOIN
                            px_cat_2_boutique ON px_categorie.id = px_cat_2_boutique.id_categorie
                        INNER JOIN
                            px_boutique ON px_boutique.id = px_cat_2_boutique.id_boutique
                        WHERE
                            px_cat_2_boutique.id_categorie = 7 AND px_boutique.url != ""
                            AND px_boutique.url != "http://" LIMIT 0,2"""
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getKeyWordFromCategorie(self, tablename):
        tablenameTmp = MySQLdb.escape_string(tablename)
        request = """   SELECT  px_motclef.motclef
                        FROM
	                            px_motclef
		                INNER JOIN
	                            px_cat_2_mc ON px_motclef.id = px_cat_2_mc.id_motclef
                        INNER JOIN
	                            px_categorie ON px_categorie.id = px_cat_2_mc.id_categorie
                        WHERE
	                            px_categorie.categorie = '""" + tablenameTmp + "'"
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getCategories(self):
        request = "SELECT C.categorie " \
                  "FROM px_cat_2_mc CM " \
                  "RIGHT JOIN px_categorie C ON C.id = CM.id_categorie " \
                  "GROUP BY C.id " \
                  "HAVING count(CM.id_motclef) >= 10"
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getCategorieFromUrl(self, url):
        request = "SELECT categorie " \
                  "FROM px_categorie " \
                  "INNER JOIN " \
                  "px_cat_2_boutique ON px_categorie.id = px_cat_2_boutique.id_categorie " \
                  "INNER JOIN px_boutique ON px_boutique.id = px_cat_2_boutique.id_boutique " \
                  "WHERE px_boutique.url = '" + url + "'"
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def getDataFromTable(self, tableName, column=None):
        if not column:
            request = "SELECT * FROM " + tableName
        else:
            request = "SELECT "+ column + " FROM " + tableName + " WHERE " + column + """ != "" """ \
                        + """ AND `url`!= "http://" AND `groupe` = 8 """
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def updatetItem(self, tablename, ProoxiItem):
        request = """UPDATE `""" + unicode(tablename, 'utf-8') + """` SET `Pourcentage`=""" + \
                  str(int(ProoxiItem['pourcentage'])) + """,`status`=""" + \
                  str(int(ProoxiItem['status'])) + """ WHERE url='""" + str(ProoxiItem['url']) + "'"
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .select(request, None)

    def moveWrongLine(self, ProoxiItem):
        request = "INSERT INTO px_boutique_tmp3(`id`,`statut`, `enseigne`,`enseigne_standard`,`groupe`,`numero_rue`," \
                  "`voie`, `nom_rue`,`complement_adresse`,`ville`,`tel`,`fax`, `courriel`,`afficher_courriel`,`url`," \
                  "`e_commerce`,`date_ajout`, `date_modif`,`nb_clics`,`aMettreEnValeur`,`nb_dead_link`,`Pourcentage`," \
                  "`status`) " \
                  "SELECT `id`,`statut`, `enseigne`,`enseigne_standard`,`groupe`, `numero_rue`,`voie`,`nom_rue`," \
                  "`complement_adresse`,`ville`,`tel`,`fax`,`courriel`,`afficher_courriel`,`url`,`e_commerce`," \
                  "`date_ajout`,`date_modif`,`nb_clics`,`aMettreEnValeur`,`nb_dead_link`,`Pourcentage`,`status`" \
                  "FROM px_boutique WHERE url ='" + str(ProoxiItem['url']) + "'"
        return MySQLConnexion.MySQLConnexion.MySQLConnexion()\
            .insert(request, None)
